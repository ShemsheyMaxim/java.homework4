package Task1;

import Task1.Literature.Book;
import Task1.Literature.Literature;
import Task1.Literature.Magazine;
import Task1.Literature.Yearbook;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.LinkedList;
import java.util.Scanner;

/**
 * Created by Maxim Shemshey on 18.05.2020.
 */

class Library {
    private LinkedList<Literature> literatures;

    LinkedList<Literature> getLiteratures() {
        return literatures;
    }

    LinkedList<Literature> initLiteratures() {
        literatures = new LinkedList<>();
        literatures.add(new Book("Ведьмин день", "Эдуард Веркин", "Эксмо", 2020));
        literatures.add(new Book("Почти родственники", "Денис Драгунский", "АСТ", 2017));
        literatures.add(new Book("Ветер сквозь замочную скважину", "Стивен Кинг", "АСТ", 2013));
        literatures.add(new Book("Зов пустоты", "Максим Шаттам", "АСТ", 2020));
        literatures.add(new Book("Человеческие поступки", "Хан Ган", "АСТ", 2015));
        literatures.add(new Magazine("GQ", "Для Мужчин", 2020, 04, 15));
        literatures.add(new Magazine("VOGUE", "Мода", 2020, 05, 6));
        literatures.add(new Magazine("Футбол", "Футбол", 2020, 04, 4));
        literatures.add(new Magazine("ШО", "Культура", 2020, 7, 21));
        literatures.add(new Magazine("Корреспондент", "Информационно-новостная", 2020, 01, 12));
        literatures.add(new Yearbook("Земля и люди", "Наука", "Мысль", 1980));
        literatures.add(new Yearbook("Земля и люди", "Наука", "Мысль", 1978));
        literatures.add(new Yearbook("Земля и люди", "Наука", "Мысль", 1979));
        literatures.add(new Yearbook("Большой советской энциклопедии", "Наука", "Москва", 1985));
        literatures.add(new Yearbook("Ежегодник Сахарной Промышленности Российской Империи", "Промышленность", "Петр Барский в Киеве", 1915));

        return literatures;
    }
}
