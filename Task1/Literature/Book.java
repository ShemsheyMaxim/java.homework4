package Task1.Literature;

/**
 * Created by Maxim Shemshey on 19.05.2020.
 */

public class Book extends Literature {
    private String author;
    private String publishingHouse;

    public Book(String name, String author, String publishingHouse, int yearPublishing) {
        super(name, yearPublishing);
        this.author = author;
        this.publishingHouse = publishingHouse;
    }

    protected String getAuthor() {
        return author;
    }

    protected String getPublishingHouse() {
        return publishingHouse;
    }

    @Override
    public String getName() {
        return super.getName();
    }

    @Override
    public int getYearPublishing() {
        return super.getYearPublishing();
    }

    @Override
    public String toString() {
        return "Книга. " + "название: '" + getName() + '\'' + ", автор: " + author + '\'' +
                ", издательство: '" + publishingHouse + '\'' + ", год публикации: " + getYearPublishing();
    }
}
