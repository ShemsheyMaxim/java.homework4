package Task1.Literature;

/**
 * Created by Maxim Shemshey on 25.05.2020.
 */
public abstract class Literature {
    private String name;
    private final int yearPublishing;

    Literature(String name, int yearPublishing) {
        this.name = name;
        this.yearPublishing = yearPublishing;
    }

    public String getName() {
        return name;
    }

    public int getYearPublishing() {
        return yearPublishing;
    }

    @Override
    public abstract String toString();
}
