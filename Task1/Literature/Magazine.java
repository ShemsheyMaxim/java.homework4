package Task1.Literature;

/**
 * Created by Maxim Shemshey on 19.05.2020.
 */
//о журналах (название, тематика, дата выхода в печать)
public class Magazine extends Literature {
    private String them;
    private final int monthOfPublishing;
    private final int dayOfMonthToPublishing;

    public Magazine(String name, String them, int yearPublishing, int monthOfPublishing, int dayOfMonthToPublishing) {
        super(name, yearPublishing);
        this.them = them;
        this.monthOfPublishing = monthOfPublishing;
        this.dayOfMonthToPublishing = dayOfMonthToPublishing;
    }

    public String getThem() {
        return them;
    }

    public int getMonthOfPublishing() {
        return monthOfPublishing;
    }

    public int getDayOfMonthToPublishing() {
        return dayOfMonthToPublishing;
    }

    @Override
    public String getName() {
        return super.getName();
    }

    @Override
    public int getYearPublishing() {
        return super.getYearPublishing();
    }

    @Override
    public String toString() {
        return "Журнал. " + "название: '" + getName() + '\'' + ", тема: '" + them + '\'' +
                ", год/месяц/день выхода в печать: " + getYearPublishing() + "/" + monthOfPublishing +
                "/" + dayOfMonthToPublishing;
    }
}
