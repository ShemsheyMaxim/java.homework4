package Task1.Literature;

/**
 * Created by Maxim Shemshey on 19.05.2020.
 */
// ежегодниках (название, тематика, издательство, год издания).
public class Yearbook extends Literature {
    private String them;
    private String publishingHouse;

    public Yearbook(String name, String them, String publishingHouse, int yearPublishing) {
        super(name, yearPublishing);
        this.them = them;
        this.publishingHouse = publishingHouse;
    }

    public String getThem() {
        return them;
    }

    public String getPublishingHouse() {
        return publishingHouse;
    }

    @Override
    public String getName() {
        return super.getName();
    }

    @Override
    public int getYearPublishing() {
        return super.getYearPublishing();
    }

    @Override
    public String toString() {
        return "Ежегодник. " + "название: '" + getName() + '\'' + ", тема: '" + them + '\'' +
                ", издательство: '" + publishingHouse + '\'' + ", год издания: " + getYearPublishing();
    }
}

