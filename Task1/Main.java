package Task1;

import Task1.Literature.Literature;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Scanner;

/**
 * Created by Maxim Shemshey on 30.05.2020.
 */
//1. Написать программу "Библиотека". Программа должна позволять хранить (в массивах или коллекциях) информацию
// о книгах (название, автор, издательство, год издания),
// о журналах (название, тематика, дата выхода в печать),
// о ежегодниках (название, тематика, издательство, год издания).
// И иметь функционал вывода в консоль списка литературы (всю доступную информацию)
// с фильтрацией по дате (за этот год, за последние 3 года, 5 лет и пр.).
public class Main {
    private static int nowYear;
    private static int searchYear;

    public static void main(String[] args) {
        printLiteratureWithFilteredByYear();
    }

    private static int getNowYear() {
        Calendar calendar = new GregorianCalendar();
        nowYear = calendar.get(Calendar.YEAR);
        return nowYear;
    }

    private static int enterAndGetSearchYear() {
        System.out.print("Введите за какой промежуток лет вы хотите получить литературу(за последний 1 год,3 года и.т.д.):");
        Scanner scan = new Scanner(System.in);
        if (scan.hasNextInt()) {
            int enterYear = scan.nextInt();
            searchYear = nowYear - enterYear;
        } else {
            System.out.println("Вы ввели не число");
            System.exit(0);
        }
        return searchYear;
    }

    private static void printLiteratureWithFilteredByYear() {
        Library library = new Library();
        library.initLiteratures();
        getNowYear();
        enterAndGetSearchYear();
        for (Literature literature : library.getLiteratures()) {
            if (searchYear < literature.getYearPublishing()) {
                System.out.println(literature);
            }
        }
    }
}
