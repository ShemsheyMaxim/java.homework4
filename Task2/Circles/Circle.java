package Task2.Circles;

import java.util.Objects;

/**
 * Created by Maxim Shemshey on 20.05.2020.
 */
public class Circle extends Ellipse {

    public Circle(double radius) {
        super("Circle", radius, radius);
    }

    @Override
    public int hashCode() {
        return getName().hashCode() + Objects.hash(minorAxisLength);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (this == obj) {
            return true;
        }
        if (this.getClass() != obj.getClass()) {
            return false;
        }
        final Circle other = (Circle) obj;
        if (this.minorAxisLength == 0 ? other.minorAxisLength != 0 : !Double.valueOf(this.minorAxisLength).equals(other.minorAxisLength)) {
            return false;
        }
        if (this.majorAxisLength == 0 ? other.majorAxisLength != 0 : !Double.valueOf(this.majorAxisLength).equals(other.majorAxisLength)) {
            return false;
        }
        return true;
    }
}
