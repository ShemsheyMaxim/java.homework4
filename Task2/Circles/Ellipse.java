package Task2.Circles;

import Task2.Figure;

import java.util.Objects;

/**
 * Created by Maxim Shemshey on 21.05.2020.
 */
public class Ellipse extends Figure {
    double minorAxisLength;
    double majorAxisLength;

    public Ellipse(double minorAxisLength, double majorAxisLength) {
        this("Ellipse", minorAxisLength, majorAxisLength);
    }

    Ellipse(String name, double minorAxisLength, double majorAxisLength) {
        super(name);
        this.minorAxisLength = minorAxisLength;
        this.majorAxisLength = majorAxisLength;
    }

    public double getMinorAxisLength() {
        return minorAxisLength;
    }

    public double getMajorAxisLength() {
        return majorAxisLength;
    }

    @Override
    protected double getSquare() {
        return majorAxisLength != 0 && minorAxisLength != 0 ? Math.PI * majorAxisLength * minorAxisLength : -1;
    }

    @Override
    protected double getPerimeter() {
        return majorAxisLength != 0 && minorAxisLength != 0 ? 2 * Math.PI * Math.sqrt(((Math.pow(majorAxisLength, 2) + Math.pow(minorAxisLength, 2))) / 2f) : -1;
    }

    @Override
    public int hashCode() {
        return getName().hashCode() + Objects.hash(minorAxisLength, majorAxisLength);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (this == obj) {
            return true;
        }
        if (this.getClass() != obj.getClass()) {
            return false;
        }
        final Ellipse other = (Ellipse) obj;
        if (this.minorAxisLength == 0 ? other.minorAxisLength != 0 : !Double.valueOf(this.minorAxisLength).equals(other.minorAxisLength)) {
            return false;
        }
        if (this.majorAxisLength == 0 ? other.majorAxisLength != 0 : !Double.valueOf(this.majorAxisLength).equals(other.majorAxisLength)) {
            return false;
        }
        return true;
    }
}
