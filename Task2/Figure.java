package Task2;

/**
 * Created by Maxim Shemshey on 20.05.2020.
 */
public abstract class Figure {

    private final String name;

    protected Figure(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    protected abstract double getSquare();

    protected abstract double getPerimeter();
}
