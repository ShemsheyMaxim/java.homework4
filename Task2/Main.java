package Task2;

import Task2.Circles.Circle;
import Task2.Circles.Ellipse;
import Task2.Polygon.Polygon;
import Task2.Polygon.Quadrilaterals.*;
import Task2.Polygon.Triangles.Triangle;

/**
 * Created by Maxim Shemshey on 20.05.2020.
 */
//  2. С использованием данного кода о фигурах (ExampleOOP) добавить класс "Многоугольник",
// который будет наследоваться от класса "Фигура" и содержать в себе массив длин его сторон.
// Заметьте, что все классы должны находится в отдельный файлах. В примере я их свел в один
// исключительно для удобства скачивания/просмотра/ознакомления.
// От этого класса должны наследоваться другие фигуры (треугольник, квадрат и пр.).
//  2.1 Дан массив разных фигур. Необходимо посчитать и вывести площадь фигур из массива,
// которые являются многоугольниками (т.е. проигнорировать круг).
//  2.2 Дан массив разных фигур. Необходимо посчитать и вывести площадь круга и тех многоугольников,
// кол-во сторон у которых больше трех.
//  2.3 Дан массив разных фигур. Необходимо посчитать и вывести периметр фигур типа "многоугольник".
//  2.4 Дан массив разных фигур. Необходимо найти в массиве одинаковые фигуры
// (у которых равны стороны или радиусы) и вывести их названия и кол-во в консоль.
//  2.5* Дан массив разных фигур. Необходимо найти фигуру, которая по длине (или ширине) больше остальных.

public class Main {

    private static Figure[] arrayFigures;

    public static void main(String[] args) {

        initArrayFigure();
        getSquarePolygonFigures(arrayFigures);
        getSquareCircleAndFigureHaveMoreThreeAngle(arrayFigures);
        getPerimeterPolygonFigures(arrayFigures);
        getEqualFigures(arrayFigures);
        getFigureWithTheBiggestSideFromQuadrilaterals(arrayFigures);
    }

    private static Figure[] initArrayFigure() {
        arrayFigures = new Figure[25];
        arrayFigures[0] = new Triangle(0, 0, 4, 5, 0, 0, 0);
        arrayFigures[1] = new Triangle(0, 4, 3, 0, 0, 0, 30);
        arrayFigures[2] = new Triangle(4, 5, 3, 0, 0, 0, 0);
        arrayFigures[3] = new Triangle(80, 100, 20, 0, 0, 0, 0);
        arrayFigures[4] = new Triangle(5, 2, 10, 0, 0, 0, 0);
        arrayFigures[5] = new Triangle(5, 4, 0, 0, 90, 0, 0);
        arrayFigures[6] = new Triangle(5, 4, 3, 0, 0, 0, 90);
        arrayFigures[7] = new Triangle(3, 3, 0, 0, 30, 0, 0);
        arrayFigures[8] = new Triangle(3, 3, 5, 0, 30, 0, 0);
        arrayFigures[9] = new Triangle(5, 5, 5, 0, 0, 0, 0);
        arrayFigures[10] = new Triangle(4, 4, 4, 0, 0, 0, 0);
        arrayFigures[11] = new Triangle(4, 4, 4, 0, 0, 0, 0);
        arrayFigures[12] = new Triangle(4, 4, 0, 5, 0, 0, 0);
        arrayFigures[13] = new Foursquare(5);
        arrayFigures[14] = new Trapezium(5, 3, 4, 3, 4);
        arrayFigures[15] = new Parallelogram(5, 6, 7);
        arrayFigures[16] = new QuadrilateralConvex(5, 4, 8, 7, 8, 4, 30);
        arrayFigures[17] = new Rectangle(3, 4);
        arrayFigures[18] = new Rhombus(11, 4);
        arrayFigures[19] = new Ellipse(5, 3);
        arrayFigures[21] = new Ellipse(5, 3);
        arrayFigures[20] = new Circle(5);
        arrayFigures[22] = new Circle(5);
        arrayFigures[23] = new QuadrilateralConvex(5, 4, 8, 7, 8, 4, 30);
        arrayFigures[24] = new QuadrilateralConvex(0, 0, 0, 0, 8, 4, 30);

        return arrayFigures;
    }

    private static void getSquarePolygonFigures(Figure[] arrayFigures) {
        for (Figure polygonFigure : arrayFigures) {
            if (polygonFigure instanceof Polygon) {
                double square = polygonFigure.getSquare();
                if (square > 0) {
                    System.out.println(String.format("Square of %s = %.2f", polygonFigure.getName(), square));
                } else if (square == -1) {
                    System.out.println(String.format("Слишком мало данных для нахождения площади %s", polygonFigure.getName()));
                } else {
                    System.out.println(String.format("%s с такими сторонами не существует", polygonFigure.getName()));
                }
            }
        }
    }

    private static void getSquareCircleAndFigureHaveMoreThreeAngle(Figure[] arrayFigures) {
        for (Figure circleAndFigureHaveMoreThreeAngle : arrayFigures) {
            if (circleAndFigureHaveMoreThreeAngle instanceof Ellipse ||
                    (circleAndFigureHaveMoreThreeAngle instanceof Polygon &&
                            ((Polygon) circleAndFigureHaveMoreThreeAngle).getCountSide() > 3)) {
                double square = circleAndFigureHaveMoreThreeAngle.getSquare();
                if (square > 0) {
                    System.out.println(String.format("Square of %s = %.2f", circleAndFigureHaveMoreThreeAngle.getName(),
                            square));
                } else if (square == -1) {
                    System.out.println(String.format("Слишком мало данных для нахождения площади %s", circleAndFigureHaveMoreThreeAngle.getName()));
                } else {
                    System.out.println(String.format("%s с такими сторонами не существует", circleAndFigureHaveMoreThreeAngle.getName()));
                }
            }
        }
    }

    private static void getPerimeterPolygonFigures(Figure[] arrayFigures) {
        for (Figure polygonFigure : arrayFigures) {
            if (polygonFigure instanceof Polygon && polygonFigure.getPerimeter() != 0) {
                double perimeter = polygonFigure.getPerimeter();
                if (perimeter > 0) {
                    System.out.println(String.format("Perimeter of %s = %.2f", polygonFigure.getName(), perimeter));
                } else if (perimeter == -1) {
                    System.out.println(String.format("Слишком мало данных для нахождения периметр %s", polygonFigure.getName()));
                } else {
                    System.out.println(String.format("%s с такими сторонами не существует", polygonFigure.getName()));
                }
            }
        }
    }

    private static void getEqualFigures(Figure[] arrayFigures) {
        int counter = 0;
        for (int i = 0; i < arrayFigures.length; i++) {
            for (int j = i + 1; j < arrayFigures.length; j++) {
                if (arrayFigures[i].hashCode() == arrayFigures[j].hashCode()) {
                    if (arrayFigures[i].equals(arrayFigures[j])) {
                        System.out.println(String.format("Фигура %d, %s = Фигуре %d, %s", i, arrayFigures[i].getName(), j,
                                arrayFigures[j].getName()));
                        counter++;
                    }
                }
            }
        }
        if (counter == 0) {
            System.out.println("Одинаковых фигур нет!");
        } else {
            System.out.println("Количество одинаковых фигур: " + counter);
        }
    }

    private static void getFigureWithTheBiggestSideFromQuadrilaterals(Figure[] arrayFigures) {
        int figureWithTheBiggestSide = 0;
        double theBiggestSide = 0;
        for (int i = 0; i < arrayFigures.length; i++) {
            if (arrayFigures[i] instanceof Quadrilateral) {
                Quadrilateral quadrilateral = (Quadrilateral) arrayFigures[i];
                for (int j = 0; j < quadrilateral.getSide().length; j++) {
                    if (theBiggestSide < quadrilateral.getSide()[j]) {
                        theBiggestSide = quadrilateral.getSide()[j];
                        figureWithTheBiggestSide = i;
                    }
                }

            }
        }
        System.out.println(String.format("Фигура с наиольшнй стороной %d, это - %s, её наибольшая сторона %.3f см.",
                figureWithTheBiggestSide, arrayFigures[figureWithTheBiggestSide].getName(), theBiggestSide));
    }
}

