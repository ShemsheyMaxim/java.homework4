package Task2.Polygon;

import Task2.Figure;

/**
 * Created by Maxim Shemshey on 22.05.2020.
 */
public abstract class Polygon extends Figure {
    protected final int countSide;

    protected Polygon(String name, int countSide) {
        super(name);
        this.countSide = countSide;
    }

    public int getCountSide() {
        return countSide;
    }
}
