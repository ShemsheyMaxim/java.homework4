package Task2.Polygon.Quadrilaterals;

import java.util.Arrays;
import java.util.Objects;

/**
 * Created by Maxim Shemshey on 20.05.2020.
 */
public class Foursquare extends Rectangle {

    public Foursquare(double side) {
        super("Foursquare", side, side);
    }

    @Override
    public int hashCode() {
        return getName().hashCode() + Objects.hash(Arrays.toString(side));
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (this == obj) {
            return true;
        }
        if (this.getClass() != obj.getClass()) {
            return false;
        }
        final Foursquare other = (Foursquare) obj;
        for (int i = 0; i < getCountSide(); i++) {
            if (this.side[i] == 0 ? other.side[i] != 0 : !Double.valueOf(this.side[i]).equals(other.side[i])) {
                return false;
            }
        }
        return true;
    }
}
