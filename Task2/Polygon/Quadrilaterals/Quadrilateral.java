package Task2.Polygon.Quadrilaterals;

import Task2.Polygon.Polygon;

import java.util.Arrays;
import java.util.Objects;

/**
 * Created by Maxim Shemshey on 21.05.2020.
 */
public abstract class Quadrilateral extends Polygon {
    final double[] side = new double[getCountSide()];

    Quadrilateral(String name, double side1, double side2, double side3, double side4) {
        super(name, 4);
        side[0] = side1;
        side[1] = side2;
        side[2] = side3;
        side[3] = side4;
    }

    public double[] getSide() {
        return side;
    }

    @Override
    protected abstract double getSquare();

    @Override
    protected double getPerimeter() {
        return side[0] != 0 && side[1] != 0 && side[2] != 0 && side[3] != 0 ? side[0] + side[1] + side[2] + side[3] : -1;
    }
}
