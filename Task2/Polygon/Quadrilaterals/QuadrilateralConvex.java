package Task2.Polygon.Quadrilaterals;

import java.util.Arrays;
import java.util.Objects;

/**
 * Created by Maxim Shemshey on 06.06.2020.
 */

public class QuadrilateralConvex extends Quadrilateral {
    private double diagonal1;
    private double diagonal2;
    private double anyAngleBetweenDiagonals;

    public QuadrilateralConvex(double side1, double side2, double side3, double side4, double diagonal1, double diagonal2, double anyAngleBetweenDiagonals) {
        this("QuadrilateralConvex", side1, side2, side3, side4, diagonal1, diagonal2, anyAngleBetweenDiagonals);
    }

    QuadrilateralConvex(String name, double side1, double side2, double side3, double side4, double diagonal1, double diagonal2, double anyAngleBetweenDiagonals) {
        super(name, side1, side2, side3, side4);
        this.diagonal1 = diagonal1;
        this.diagonal2 = diagonal2;
        this.anyAngleBetweenDiagonals = anyAngleBetweenDiagonals;
    }

    public double getDiagonal1() {
        return diagonal1;
    }

    public double getDiagonal2() {
        return diagonal2;
    }

    public double getAnyAngleBetweenDiagonals() {
        return anyAngleBetweenDiagonals;
    }

    @Override
    protected double getSquare() {
        return diagonal1 != 0 && diagonal2 != 0 && anyAngleBetweenDiagonals != 0 ? (1 / 2f) * diagonal1 * diagonal2 * Math.sin(Math.toRadians(anyAngleBetweenDiagonals)) : -1;
    }

    @Override
    public int hashCode() {
        return getName().hashCode() + Objects.hash(diagonal1, diagonal2, anyAngleBetweenDiagonals);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (this == obj) {
            return true;
        }
        if (this.getClass() != obj.getClass()) {
            return false;
        }
        final QuadrilateralConvex other = (QuadrilateralConvex) obj;
        for (int i = 0; i < getCountSide(); i++) {
            if (this.side[i] == 0 ? other.side[i] != 0 : !Double.valueOf(this.side[i]).equals(other.side[i])) {
                return false;
            }
        }
        if (this.diagonal1 == 0 ? other.diagonal1 != 0 : !Double.valueOf(this.diagonal1).equals(other.diagonal1)) {
            return false;
        }
        if (this.diagonal2 == 0 ? other.diagonal2 != 0 : !Double.valueOf(this.diagonal2).equals(other.diagonal2)) {
            return false;
        }
        if (this.anyAngleBetweenDiagonals == 0 ? other.anyAngleBetweenDiagonals != 0 : !Double.valueOf(this.anyAngleBetweenDiagonals).equals(other.anyAngleBetweenDiagonals)) {
            return false;
        }
        return true;
    }
}
