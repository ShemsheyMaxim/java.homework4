package Task2.Polygon.Quadrilaterals;

import java.util.Arrays;
import java.util.Objects;

/**
 * Created by Maxim Shemshey on 20.05.2020.
 */
public class Rectangle extends Parallelogram {

    public Rectangle(double length, double width) {
        this("Rectangle", length, width);
    }

    Rectangle(String name, double length, double width) {
        super(name, length, width, length, width, width);
    }

    @Override
    protected double getSquare() {
        return side[0] != 0 && side[1] != 0 ? side[0] * side[1] : -1;
    }

    @Override
    public int hashCode() {
        return getName().hashCode() + Objects.hash(Arrays.toString(side));
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (this == obj) {
            return true;
        }
        if (this.getClass() != obj.getClass()) {
            return false;
        }
        final Rectangle other = (Rectangle) obj;
        for (int i = 0; i < getCountSide(); i++) {
            if (this.side[i] == 0 ? other.side[i] != 0 : !Double.valueOf(this.side[i]).equals(other.side[i])) {
                return false;
            }
        }
        return true;
    }
}


