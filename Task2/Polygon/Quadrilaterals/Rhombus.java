package Task2.Polygon.Quadrilaterals;

import java.util.Arrays;
import java.util.Objects;

/**
 * Created by Maxim Shemshey on 21.05.2020.
 */
public class Rhombus extends Parallelogram {

    public Rhombus(double base, double height) {
        this("Rhombus", base, height);
    }

    private Rhombus(String name, double base, double height) {
        super(name, base, base, base, base, height);
    }

    @Override
    public int hashCode() {
        return getName().hashCode() + Objects.hash(Arrays.toString(side), height);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (this == obj) {
            return true;
        }
        if (this.getClass() != obj.getClass()) {
            return false;
        }
        final Rhombus other = (Rhombus) obj;
        for (int i = 0; i < getCountSide(); i++) {
            if (this.side[i] == 0 ? other.side[i] != 0 : !Double.valueOf(this.side[i]).equals(((Rhombus) obj).side[i])) {
                return false;
            }
        }
        if (this.height == 0 ? other.height != 0 : !Double.valueOf(this.height).equals(other.height)) {
            return false;
        }
        return true;
    }
}
