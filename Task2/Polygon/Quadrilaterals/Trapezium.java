package Task2.Polygon.Quadrilaterals;

import java.util.Arrays;
import java.util.Objects;

/**
 * Created by Maxim Shemshey on 21.05.2020.
 */
public class Trapezium extends QuadrilateralConvex {
    private final double height;

    public Trapezium(double base1, double base2, double side3, double side4, double height) {
        super("Trapezium", base1, base2, side3, side4, 0, 0, 0);
        this.height = height;
    }

    public double getHeight() {
        return height;
    }

    @Override
    protected double getSquare() {
        return side[0] != 0 && side[1] != 0 && height != 0 ? ((side[0] + side[1]) / 2f) * height : -1;
    }

    @Override
    public int hashCode() {
        return getName().hashCode() + Objects.hash(Arrays.toString(side), height);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (this == obj) {
            return true;
        }
        if (this.getClass() != obj.getClass()) {
            return false;
        }
        final Trapezium other = (Trapezium) obj;
        for (int i = 0; i < getCountSide(); i++) {
            if (this.side[i] == 0 ? other.side[i] != 0 : !Double.valueOf(this.side[i]).equals(other.side[i])) {
                return false;
            }
        }
        if (this.height == 0 ? other.height != 0 : Double.valueOf(this.height).equals(other.height)) {
            return false;
        }
        return true;
    }
}
