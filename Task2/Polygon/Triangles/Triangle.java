package Task2.Polygon.Triangles;

import Task2.Polygon.Polygon;

import java.util.Arrays;
import java.util.Objects;

/**
 * Created by Maxim Shemshey on 20.05.2020.
 */
public class Triangle extends Polygon {
    private double[] side = new double[getCountSide()];
    private final double height;
    private final double angleBetweenSide1AndSide2;
    private final double angleBetweenSide1AndBase;
    private final double angleBetweenSide2AndBase;

    public Triangle(double side1, double side2, double base, double height,
                    double angleBetweenSide1AndSide2, double angleBetweenSide1AndBase, double angleBetweenSide2AndBase) {
        super("Triangle", 3);
        side[0] = side1;
        side[1] = side2;
        side[2] = base;
        this.height = height;
        this.angleBetweenSide1AndSide2 = angleBetweenSide1AndSide2;
        this.angleBetweenSide1AndBase = angleBetweenSide1AndBase;
        this.angleBetweenSide2AndBase = angleBetweenSide2AndBase;
    }

    @Override
    public String getName() {
        return super.getName();
    }

    public double getHeight() {
        return height;
    }

    public double getAngleBetweenSide1AndSide2() {
        return angleBetweenSide1AndSide2;
    }

    public double getAngleBetweenSide1AndBase() {
        return angleBetweenSide1AndBase;
    }

    public double getAngleBetweenSide2AndBase() {
        return angleBetweenSide2AndBase;
    }

    @Override
    protected double getSquare() {
        if (side[0] == 0 && side[1] == 0 && side[2] != 0 && height != 0) {
            return calculateSquareForTriangleWithBaseAndHeight(side[2], height);
        } else if (side[0] == 0 && side[2] == 0 && side[1] != 0 && height != 0) {
            return calculateSquareForTriangleWithBaseAndHeight(side[1], height);
        } else if (side[1] == 0 && side[2] == 0 && side[0] != 0 && height != 0) {
            return calculateSquareForTriangleWithBaseAndHeight(side[0], height);
        } else if (angleBetweenSide1AndSide2 != 0 && side[0] != 0 && side[1] != 0 && side[2] == 0) {
            return angleBetweenSide1AndSide2 == 90 ?
                    calculateSquareForTriangleRight(side[0], side[1]) :
                    calculateSquareForTriangleWith2SideAndAngleBetweenThem(side[0], side[1], angleBetweenSide1AndSide2);
        } else if (angleBetweenSide1AndBase != 0 && side[0] != 0 && side[2] != 0 && side[1] == 0) {
            return angleBetweenSide1AndBase == 90 ? calculateSquareForTriangleRight(side[0], side[2]) :
                    calculateSquareForTriangleWith2SideAndAngleBetweenThem(side[0], side[2], angleBetweenSide1AndBase);
        } else if (angleBetweenSide2AndBase != 0 && side[1] != 0 && side[2] != 0 && side[0] == 0) {
            return angleBetweenSide2AndBase == 90 ? calculateSquareForTriangleRight(side[1], side[2]) :
                    calculateSquareForTriangleWith2SideAndAngleBetweenThem(side[1], side[2], angleBetweenSide2AndBase);
        } else if ((side[0] != 0 && side[1] != 0 && side[2] == 0 && side[0] == side[1] && height != 0)
                || (side[0] != 0 && side[2] != 0 && side[1] == 0 && side[0] == side[2] && height != 0)
                || (side[1] != 0 && side[2] != 0 && side[0] == 0 && side[1] == side[2] && height != 0)) {
            return calculateSquareForTriangleIsoscelesWithHeight(height);
        } else if (side[0] != 0 && side[1] != 0 && side[2] != 0) {
            return calculateSquareForTriangleFormulaHeron(side[0], side[1], side[2]);
        }
        return -1;
    }

    private double calculateSquareForTriangleIsoscelesWithHeight(double height) {
        return (Math.pow(height, 2)) / (Math.sqrt(3f));
    }

    private double calculateSquareForTriangleFormulaHeron(double side1, double side2, double side3) {
        double p = (side1 + side2 + side3) / 2f;
        return Math.sqrt(p * (p - side1) * (p - side2) * (p - side3));
    }

    private double calculateSquareForTriangleRight(double side1, double side2) {
        return (1f / 2f) * side1 * side2;
    }

    private double calculateSquareForTriangleWithBaseAndHeight(double base, double height) {
        return 1f / 2f * base * height;
    }


    private double calculateSquareForTriangleWith2SideAndAngleBetweenThem(double side1, double side2, double angleBetweenSide1AndSide2) {
        return 1f / 2f * (side1 * side2 * (Math.sin(Math.toRadians(angleBetweenSide1AndSide2))));
    }

    @Override
    protected double getPerimeter() {

        if (side[0] != 0 && side[1] != 0 && side[2] != 0) {
            return countPerimeterForTriangleFormulaHeron(side[0], side[1], side[2]);
        } else if (angleBetweenSide1AndSide2 != 0 && side[0] != 0 && side[1] != 0) {
            return angleBetweenSide1AndSide2 == 90 ?
                    countPerimeterForTwoSidesAndRectangularAngleBetweenThem(side[0], side[1]) :
                    countPerimeterForTwoSidesAndAngleBetweenThem(side[0], side[1], angleBetweenSide1AndSide2);
        } else if (angleBetweenSide1AndBase != 0 && side[0] != 0 && side[2] != 0) {
            return angleBetweenSide1AndBase == 90 ?
                    countPerimeterForTwoSidesAndRectangularAngleBetweenThem(side[0], side[2]) :
                    countPerimeterForTwoSidesAndAngleBetweenThem(side[0], side[2], angleBetweenSide1AndBase);
        } else if (angleBetweenSide2AndBase != 0 && side[1] != 0 && side[2] != 0) {
            return angleBetweenSide2AndBase == 90 ?
                    countPerimeterForTwoSidesAndRectangularAngleBetweenThem(side[1], side[2]) :
                    countPerimeterForTwoSidesAndAngleBetweenThem(side[1], side[2], angleBetweenSide2AndBase);
        } else {
            return -1;
        }
    }

    private double countPerimeterForTriangleFormulaHeron(double side0, double side1, double side2) {
        return side0 + side1 + side2;
    }

    private double countPerimeterForTwoSidesAndRectangularAngleBetweenThem(double side0, double side1) {
        return countPerimeterForTriangleFormulaHeron(side0, side1, countSide3ForTwoSidesAndRectangularAngleBetweenThem(side0, side1));
    }

    private double countSide3ForTwoSidesAndRectangularAngleBetweenThem(double side0, double side1) {
        return Math.sqrt((Math.pow(side0, 2)) + (Math.pow(side1, 2)));
    }

    private double countPerimeterForTwoSidesAndAngleBetweenThem(double side0, double side1, double angle) {
        return countPerimeterForTriangleFormulaHeron(side0, side1, countSide3ForTwoSidesAndAngleBetweenThem(side0, side1, angle));
    }

    private double countSide3ForTwoSidesAndAngleBetweenThem(double side0, double side1, double angle) {
        return Math.sqrt(((Math.pow(side0, 2)) + (Math.pow(side1, 2))) - 2 * (side0 * side1 * (Math.cos(Math.toRadians(angle)))));
    }

    @Override
    public int hashCode() {
        return getName().hashCode() + Objects.hash(Arrays.toString(side), height, getCountSide(),
                angleBetweenSide1AndSide2, angleBetweenSide1AndBase);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (this == obj) {
            return true;
        }
        if (this.getClass() != obj.getClass()) {
            return false;
        }
        final Triangle other = (Triangle) obj;
        for (int i = 0; i < getCountSide(); i++) {
            if (this.side[i] == 0 ? other.side[i] != 0 : !Double.valueOf(this.side[i]).equals(other.side[i])) {
                return false;
            }
        }
        if (this.height == 0 ? other.height != 0 : Double.valueOf(this.height).equals(other.height)) {
            return false;
        }
        if (this.angleBetweenSide1AndSide2 == 0 ? other.angleBetweenSide1AndSide2 != 0 :
                Double.valueOf(this.angleBetweenSide1AndSide2).equals(other.angleBetweenSide1AndSide2)) {
            return false;
        }
        if (this.angleBetweenSide1AndBase == 0 ? other.angleBetweenSide1AndBase != 0 :
                Double.valueOf(this.angleBetweenSide1AndBase).equals(other.angleBetweenSide1AndBase)) {
            return false;
        }
        if (this.angleBetweenSide2AndBase == 0 ? other.angleBetweenSide2AndBase != 0 :
                Double.valueOf(this.angleBetweenSide2AndBase).equals(other.angleBetweenSide2AndBase)) {
            return false;
        }
        return true;
    }
}
