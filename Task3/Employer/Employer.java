package Task3.Employer;

/**
 * Created by Maxim Shemshey on 24.05.2020.
 */
public abstract class Employer {
    private String name;
    private String surname;
    private String patronymic;
    private String position;

    Employer(String name, String surname, String patronymic, String position) {
        this.name = name;
        this.surname = surname;
        this.patronymic = patronymic;
        this.position = position;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public String getPatronymic() {
        return patronymic;
    }

    public String getPosition() {
        return position;
    }

    protected abstract double getMonthlyIncome();

    protected abstract double getAnnualIncome();

    @Override
    public abstract String toString();
}
