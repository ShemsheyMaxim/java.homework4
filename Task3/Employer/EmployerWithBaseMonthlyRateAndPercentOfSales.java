package Task3.Employer;

/**
 * Created by Maxim Shemshey on 24.05.2020.
 */
public class EmployerWithBaseMonthlyRateAndPercentOfSales extends Employer {
    private final int monthlyRate;
    private final int percentOfSales;
    private final int salesForTheFirstSixMonths;
    private final int salesForTheSecondSixMonths;

    public EmployerWithBaseMonthlyRateAndPercentOfSales(String name, String surname, String patronymic,
                                                        String position, int monthlyRate, int percentOfSales,
                                                        int salesForTheFirstSixMonths, int salesForTheSecondSixMonths) {
        super(name, surname, patronymic, position);
        this.monthlyRate = monthlyRate;
        this.percentOfSales = percentOfSales;
        this.salesForTheFirstSixMonths = salesForTheFirstSixMonths;
        this.salesForTheSecondSixMonths = salesForTheSecondSixMonths;
    }

    public int getMonthlyRate() {
        return monthlyRate;
    }

    public int getPercentOfSales() {
        return percentOfSales;
    }

    public int getSalesForTheFirstSixMonths() {
        return salesForTheFirstSixMonths;
    }

    public int getSalesForTheSecondSixMonths() {
        return salesForTheSecondSixMonths;
    }

    @Override
    public String getName() {
        return super.getName();
    }

    @Override
    public String getSurname() {
        return super.getSurname();
    }

    @Override
    public String getPatronymic() {
        return super.getPatronymic();
    }

    @Override
    public String getPosition() {
        return super.getPosition();
    }

    @Override
    protected double getMonthlyIncome() {
        return ((salesForTheFirstSixMonths + salesForTheSecondSixMonths) * (percentOfSales * 0.01f) / 2f) + monthlyRate;
    }

    @Override
    protected double getAnnualIncome() {
        return (((salesForTheFirstSixMonths * 6) + (salesForTheSecondSixMonths * 6)) * (percentOfSales * 0.01f)) + (monthlyRate * 12);
    }

    @Override
    public String toString() {
        return getName() + " " + getSurname() + " " + getPatronymic() + ", " +
                getPosition() + ", " + "оклад: " + String.format("%.2f", getMonthlyIncome()) + "грн, " +
                "годовой оклад: " + String.format("%.2f", getAnnualIncome()) + "грн.";
    }
}
