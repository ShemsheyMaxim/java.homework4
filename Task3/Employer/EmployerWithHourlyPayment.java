package Task3.Employer;

/**
 * Created by Maxim Shemshey on 24.05.2020.
 */
public class EmployerWithHourlyPayment extends Employer {
    private final int hourlyPayment;


    public EmployerWithHourlyPayment(String name, String surname, String patronymic, String position, int hourlyPayment) {
        super(name, surname, patronymic, position);
        this.hourlyPayment = hourlyPayment;
    }

    public int getHourlyPayment() {
        return hourlyPayment;
    }

    @Override
    public String getName() {
        return super.getName();
    }

    @Override
    public String getSurname() {
        return super.getSurname();
    }

    @Override
    public String getPatronymic() {
        return super.getPatronymic();
    }

    @Override
    public String getPosition() {
        return super.getPosition();
    }

    @Override
    protected double getMonthlyIncome() {
        return hourlyPayment * 8 * 5 * 4;
    }

    @Override
    protected double getAnnualIncome() {
        return hourlyPayment * 40 * 4 * 12;
    }

    @Override
    public String toString() {
        return getName() + " " + getSurname() + " " + getPatronymic() + ", " +
                getPosition() + ", " + "оклад: " + getMonthlyIncome() + "грн, " +
                "годовой оклад: " + getAnnualIncome() + "грн.";
    }
}
