package Task3.Employer;

/**
 * Created by Maxim Shemshey on 24.05.2020.
 */
public class EmployerWithMonthlyRate extends Employer {
    private final int monthlyRate;

    public EmployerWithMonthlyRate(String name, String surname, String patronymic, String position, int monthlyRate) {
        super(name, surname, patronymic, position);
        this.monthlyRate = monthlyRate;
    }

    public int getMonthlyRate() {
        return monthlyRate;
    }

    @Override
    public String getName() {
        return super.getName();
    }

    @Override
    public String getSurname() {
        return super.getSurname();
    }

    @Override
    public String getPatronymic() {
        return super.getPatronymic();
    }

    @Override
    public String getPosition() {
        return super.getPosition();
    }

    @Override
    protected double getMonthlyIncome() {
        return monthlyRate;
    }

    @Override
    protected double getAnnualIncome() {
        return monthlyRate * 12;
    }

    @Override
    public String toString() {
        return getName() + " " + getSurname() + " " + getPatronymic() + ", " +
                getPosition() + ", " + "оклад: " + getMonthlyIncome() + "грн, " +
                "годовой оклад: " + getAnnualIncome() + "грн.";
    }
}
