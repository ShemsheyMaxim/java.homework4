package Task3.Employer;

/**
 * Created by Maxim Shemshey on 24.05.2020.
 */
//  Коренькова Анна Павловна, менеджер по продажам (получает 5% от продаж,
//  первые полгода касса составляла 50000грн, вторые полгода - 65000грн в месяц)

public class EmployerWithPercentOfSales extends Employer {
    private final int percentOfSales;
    private final int salesForTheFirstSixMonths;
    private final int salesForTheSecondSixMonths;

    public EmployerWithPercentOfSales(String name, String surname, String patronymic, String position, int percentOfSales,
                                      int salesForTheFirstSixMonths, int salesForTheSecondSixMonths) {
        super(name, surname, patronymic, position);
        this.percentOfSales = percentOfSales;
        this.salesForTheFirstSixMonths = salesForTheFirstSixMonths;
        this.salesForTheSecondSixMonths = salesForTheSecondSixMonths;
    }

    public int getPercentOfSales() {
        return percentOfSales;
    }

    public int getSalesForTheFirstSixMonths() {
        return salesForTheFirstSixMonths;
    }

    public int getSalesForTheSecondSixMonths() {
        return salesForTheSecondSixMonths;
    }

    @Override
    public String getName() {
        return super.getName();
    }

    @Override
    public String getSurname() {
        return super.getSurname();
    }

    @Override
    public String getPatronymic() {
        return super.getPatronymic();
    }

    @Override
    public String getPosition() {
        return super.getPosition();
    }

    @Override
    protected double getMonthlyIncome() {
        return (salesForTheFirstSixMonths + salesForTheSecondSixMonths) * (percentOfSales * 0.01f) / 2f;
    }

    @Override
    protected double getAnnualIncome() {
        return ((salesForTheFirstSixMonths * 6) + (salesForTheSecondSixMonths * 6)) * (percentOfSales * 0.01f);
    }

    @Override
    public String toString() {
        return getName() + " " + getSurname() + " " + getPatronymic() + ", " +
                getPosition() + ", " + "оклад: " + String.format("%.2f", getMonthlyIncome()) + "грн, " +
                "годовой оклад: " + String.format("%.2f", getAnnualIncome()) + "грн.";
    }
}
