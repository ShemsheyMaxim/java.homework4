package Task3;

import Task3.Employer.*;

/**
 * Created by Maxim Shemshey on 24.05.2020.
 */
//3.Написать программу "Бухгалтерия". Программа должна позволять рассчитать месячный и годовой доход
//  каждого сотрудника, в зависимости от расчёта заработной платы. А именно:
//  - месячная ставка
//  - почасовая оплата
//  - % от объема продаж
//  - базовая месячная ставка + % от объема продаж
//  Для каждого сотрудника необходимо хранить ФИО и должность.
//  Проверить работу программы необходимо на следующих сотрудниках:
//  Иванова Елена Львовна, зам директора, оклад 4500грн
//  Вакуленко Дмитрий Владимирович, дизайнер, 7$ в час
//  Коренькова Анна Павловна, менеджер по продажам (получает 5% от продаж,
//  первые полгода касса составляла 50000грн, вторые полгода - 65000грн в месяц)
//  Татьяна Сергеевна, менеджер по продажам (получает 1000грн + 3% от продаж, касса аналогична Кореньковой Анне).

public class Main {

    public static void main(String[] args) {
        Bookkeeping bookkeeping = new Bookkeeping();
        bookkeeping.initArrayOfEmployer();
        bookkeeping.getEmployers();
    }

    private static class Bookkeeping {
        private Employer[] employers;

        private Employer[] initArrayOfEmployer() {
            employers = new Employer[4];
            employers[0] = new EmployerWithMonthlyRate("Иванова", "Елена", "Львовна", "зам директора", 4500);
            employers[1] = new EmployerWithHourlyPayment("Вакуленко", "Дмитрий", "Владимирович", "дизайнер", 7);
            employers[2] = new EmployerWithPercentOfSales("Коренькова", "Анна", "Павловна", "менеджер по продажам", 5, 50000, 65000);
            employers[3] = new EmployerWithBaseMonthlyRateAndPercentOfSales("Татьяна", "", "Сергеевна", "менеджер по продажам", 1000, 3, 50000, 65000);
            return employers;
        }

        private void getEmployers() {
            for (Employer emp : employers) {
                System.out.println(emp.toString());
            }
        }
    }
}


